# -*- coding: utf-8 -*-

PLUGIN_NAME = u"Detele Files"
PLUGIN_AUTHOR = u"Ovidiu-Florin Bogdan"
PLUGIN_DESCRIPTION = "Adds a context menu item that gives the possibility to delete the selected file(s) or track(s) from the filesystem."
PLUGIN_VERSION = "0.1"
PLUGIN_API_VERSIONS = ["1.0.0"]

import os
from picard.file import File
from picard.track import Track
from picard.ui.itemviews import BaseAction, register_file_action

class DeleteTheSelectedFiles(BaseAction):
	NAME = "Delete from filesystem"
	
	def callback(self, objs):
		if len(objs) < 1:
			return
		
		for obj in objs:
			if isinstance(obj, File):
				os.remove(obj.filename)
				obj.remove()
			elif isinstance(obj, Track):
				for linked in obj.linked_files:
					os.remove(linked.filemane)
					linked.remove()
				
register_file_action(DeleteTheSelectedFiles())
